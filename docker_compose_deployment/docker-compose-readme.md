# Compliance Hub Docker Compose Deployment Example

This folder contains a docker compose YAML file `docker-compose.yaml` which provides an example
of how to deploy the Compliance Hub containers with docker compose. This deploys four containers
for the Compliance Hub client, gateway, clamAV and controller.

This example includes an additional proxy server container (named `proxy`) which is used to direct
user traffic to the compliance-hub-client and compliance-hub-gateway containers. This can be
replaced with your own load-balancer to redirect traffic to the appropriate container if desired.
See the file `nginx.conf` for the redirect rules that need to be added to your load-balancer.

Users only need access to the proxy container and Pipeline Pilot server. All other traffic can use
private non user-facing networks. The example docker-compose file uses two networks `frontend`
which is the public/user-facing network which contains the compliance-hub-proxy and `backend`
which is the private non user-facing network where internal container-container communication
occurs.

## Deployment prerequisites

- An .env file with the required environment variables set. See the
[example.env](/docker_compose_deployment/example.env) file in this folder. Scitegrity will work
with you to generate the required environment variables for your deployment.

- If you are using the optional compliance-hub-proxy container, SSL certificates will also need
to be put in the `/certs` folder with the names `ssl.crt` and `ssl.key`.

## Deploying Compliance Hub using Docker Compose

To spin up the containers in docker run this command in the `docker_compose_deployment` directory.
The --env-file argument should point to location of file where the required environment variables
have been set.

```docker compose --env-file ./example.env up --build```
