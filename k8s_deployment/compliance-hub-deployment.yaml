# Deployment for client pod
apiVersion: apps/v1
kind: Deployment
metadata:
  name: compliance-hub-client-deployment
  labels:
    app: compliance-hub-client
spec: 
  selector:
    matchLabels:
      app: compliance-hub-client
  template:
    metadata:
      labels:
        app: compliance-hub-client
        app.kubernetes.io/name: compliance-hub-client
    spec:
      restartPolicy: Always
      containers:
      - name: client
        image: registry.gitlab.com/scitegrityimages/compliance-hub-client:v6.2.8
        ports:
          - containerPort: 8081
            protocol: TCP
        livenessProbe:
          httpGet:
            path: /health
            port: 8081
          initialDelaySeconds: 15
          periodSeconds: 15
          failureThreshold: 4
          timeoutSeconds: 60

---
# Deployment for clamAV pod
apiVersion: apps/v1
kind: Deployment
metadata:
  name: compliance-hub-clamav-deployment
  labels:
    app: compliance-hub-clamav
spec: 
  selector:
    matchLabels:
      app: compliance-hub-clamav
  template:
    metadata:
      labels:
        app: compliance-hub-clamav
        app.kubernetes.io/name: compliance-hub-clamav
    spec:
      restartPolicy: Always
      containers:
      - name: clamav
        image: registry.gitlab.com/scitegrityimages/compliance-hub-clamav:v1.0
        ports:
          - containerPort: 3310
            protocol: TCP

---
# Deployment for gateway pod
apiVersion: apps/v1
kind: Deployment
metadata:
  name: compliance-hub-gateway-deployment
  labels:
    app: compliance-hub-gateway
spec: 
  selector:
    matchLabels:
      app: compliance-hub-gateway
  template:
    metadata:
      labels:
        app: compliance-hub-gateway
        app.kubernetes.io/name: compliance-hub-gateway
    spec:
      restartPolicy: Always
      containers:
      - name: gateway
        image: registry.gitlab.com/scitegrityimages/compliance-hub-gateway:v6.2
        ports:
          - containerPort: 8080
            protocol: TCP
        livenessProbe:
          httpGet:
            path: /health
            port: 8080
          initialDelaySeconds: 15
          periodSeconds: 15
          failureThreshold: 4
          timeoutSeconds: 60
        env:
          - name: AUTH_BACKEND
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: AUTH_BACKEND
          - name: CLAMAV_HOST
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: CLAMAV_HOST
          - name: CLAMAV_PORT
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: CLAMAV_PORT
          - name: COMPLIANCE_HUB_HOSTNAME
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: COMPLIANCE_HUB_HOSTNAME
          - name: CS2GATEWAY_LISTEN_PORT
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: CS2GATEWAY_LISTEN_PORT
          - name: CS2GATEWAY_ORACLEDB_CONNECTIONSTRING
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: CS2GATEWAY_ORACLEDB_CONNECTIONSTRING
          - name: CS2GATEWAY_ORACLEDB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: compliance-hub-secrets
                key: CS2GATEWAY_ORACLEDB_PASSWORD
          - name: CS2GATEWAY_ORACLEDB_USER
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: CS2GATEWAY_ORACLEDB_USER
          - name: JWT_EXPIRY
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: JWT_EXPIRY
          - name: JWT_SECRET_KEY
            valueFrom:
              secretKeyRef:
                name: compliance-hub-secrets
                key: JWT_SECRET_KEY
          - name: PIPELINE_PILOT_HOST
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: PIPELINE_PILOT_HOST
          - name: PIPELINE_PILOT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: compliance-hub-secrets
                key: PIPELINE_PILOT_PASSWORD
          - name: PIPELINE_PILOT_PORT
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: PIPELINE_PILOT_PORT
          - name: PIPELINE_PILOT_USER
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: PIPELINE_PILOT_USER
          - name: PROCESS_CONTROLLER_SECRET
            valueFrom:
              secretKeyRef:
                name: compliance-hub-secrets
                key: PROCESS_CONTROLLER_SECRET
          - name: UI_URL
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: UI_URL
          - name: PLAIN_HTTP
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: PLAIN_HTTP

---
# Deployment for controller pod
apiVersion: apps/v1
kind: Deployment
metadata:
  name: compliance-hub-controller-deployment
  labels:
    app: compliance-hub-controller
spec: 
  selector:
    matchLabels:
      app: compliance-hub-controller
  template:
    metadata:
      labels:
        app: compliance-hub-controller
        app.kubernetes.io/name: compliance-hub-controller
    spec:
      restartPolicy: Always
      containers:
      - name: controller
        image: registry.gitlab.com/scitegrityimages/compliance-hub-controller:v6.1
        env:
          - name: GATEWAY_HOST
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: GATEWAY_HOST
          - name: GATEWAY_JOB_RETENTION_TIME
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: GATEWAY_JOB_RETENTION_TIME
          - name: GATEWAY_PASSWORD
            valueFrom:
              secretKeyRef:
                name: compliance-hub-secrets
                key: PROCESS_CONTROLLER_SECRET
          - name: GATEWAY_PORT
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: GATEWAY_PORT
          - name: PIPELINE_PILOT_HOST
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: PIPELINE_PILOT_HOST
          - name: PIPELINE_PILOT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: compliance-hub-secrets
                key: PIPELINE_PILOT_PASSWORD
          - name: PIPELINE_PILOT_PORT
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: PIPELINE_PILOT_PORT
          - name: PIPELINE_PILOT_USER
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: PIPELINE_PILOT_USER
          - name: DISABLE_SSL_CERT_CHECK
            valueFrom:
              configMapKeyRef:
                name: compliance-hub-configmap
                key: DISABLE_SSL_CERT_CHECK
