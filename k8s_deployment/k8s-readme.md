# Compliance Hub Kubernetes Deployment Example

This folder contains files for an example Kubernetes deployment for Compliance Hub. This creates
a Kubernetes cluster with 4 pods for the client, gateway, clamAV and controller.

There are 6 YAML files:

- `compliance-hub-config-map.yaml`: K8s config map which stores environment variables needed for
Compliance Hub containers
- `compliance-hub-secrets.yaml`: K8s secrets file which stores secret environment variables needed
for Compliance Hub containers
- `secret-tls.yaml`: K8s secrets file which stores SSL certificate information used by the K8s load
balancer
- `compliance-hub-service.yaml`: defines services required for network access to client, gateway
and clamAV pods
- `compliance-hub-gateway.yaml`: defines K8s gateway, loadbalancer and routing rules for user
traffic to the pods
- `compliance-hub-deployment.yaml`: contains deployment definitions for each of the Compliance Hub
pods

## Deployment prerequisites

- Update `compliance-hub-config-map.yaml` and `compliance-hub-secrets.yaml` with the env vars
needed for your deployment
- Update `secret-tls.yaml` with the SSL certificates matching the domain this will be deployed
to
- This example uses Kubernetes gateway-API [link here](https://gateway-api.sigs.k8s.io/) for
traffic routing with the Envoy gateway controller. If a different gateway controller (e.g. Nginx,
Istio) is being used, the `compliance-hub-gateway.yaml` will need to be updated accordingly

## Creating the Compliance Hub K8s cluster

To create the cluster in a K8s development environment run the following commands:

```bash
# create config map and secrets used to set env vars
kubectl apply -f compliance-hub-config-map.yaml
kubectl apply -f compliance-hub-secrets.yaml
kubectl apply -f secret-tls.yaml

# create services
kubectl apply -f compliance-hub-service.yaml

# create gateway/loadbalancer for cluster
kubectl apply -f https://github.com/kubernetes-sigs/gateway-api/releases/download/v1.2.0/standard-install.yaml # install K8s gateway API
kubectl apply --server-side -f https://github.com/envoyproxy/gateway/releases/download/v1.2.4/install.yaml # install envoy gateway-controller
kubectl apply -f compliance-hub-gateway.yaml

# create deployments
kubectl apply -f compliance-hub-deployment.yaml
```

To delete/clean-up all K8s resources created in a developement environment run the following command:

```bash
# cleanup all K8s resources created (when testing locally)
kubectl delete all --all
```
