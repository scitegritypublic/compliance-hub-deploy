# compliance-hub-deploy

This respository contains examples configurations for deploying Compliance Hub containers using
Kubernetes(see `k8_deployment` folder) or Docker Compose (see `docker_compose_deployment` folder)
and guidance for deploying containers with AWS ECS (see `aws_deployment_guidance` folder). These
containers are used to serve the Compliance Hub APIs and web application to users.

There are four containers required for Compliance Hub:

- **compliance-hub-client:** This container serves the Scitegrity Compliance Hub web application
(including Controlled Substances Squared) to users. Users are directed to this container when they
access the CS2 UI.

- **compliance-hub-gateway:** This serves the APIs required for the new Scitegrity user interface
and webservice endpoints. The container makes calls to Pipeline Pilot for authenticating users and
when users use Pipette sketch to draw in chemistry. It also makes calls the CS2 Oracle database.

- **compliance-hub-clamav:** The compliance-hub-gateway service uses this container service to virus
scan user input.

- **compliance-hub-controller:** This container runs a scheduled job every 5 mins to clean up
expired jobs and check the Pipeline Pilot protocols that execute user jobs are running. It doesn’t
accept any traffic any only makes calls to the compliance-hub-gateway container and the Pipeline
Pilot server.

Users interact with a proxy/load balancer which routes their requests to the appropriate
container/pod. This can be a Kubernetes gateway (see Kubernetes example), a separate container
running a proxy server (see docker compose example) or your own load balancer.

Here is a diagram showing how the services are architected and the data flow between the containers:
![Compliance Hub Architecture Schematic](/assets/deploymentSchematic.png).

## Deployment prerequisites

- A login for Scitegrity docker repository - a username/password is required to download Compliance
Hub container images. This will be provided by Scitegrity. You can log into our docker repository
using the following command (where `<<username>>` and `<<password>>` are thee credentials supplied
by Scitegrity):

   ```docker login -u <<username>> -p <<password>> registry.gitlab.com```

- SSL certificates for the domain you wish to deploy in.

## Deploying Compliance Hub using Kubernetes (recommended option)

To deploy Compliance Hub in a Kubernetes cluster please find example deployment YAML files in the
`k8_deployment` folder of this repository. The [k8s_readme.md](/k8s_deployment/k8s-readme.md) will
provide instructions on how to use these files to deploy Compliance Hub into your cluster.

## Deploying Compliance Hub using Docker Compose

To deploy Compliance Hub containers using docker find an example docker compose YAML file in the
`docker_compose_deployment` folder of this repository. The
[docker-compose-readme.md](/docker_compose_deployment/docker-compose-readme.md) will
provide deployment instructions.

## Deploying Compliance Hub in AWS

We also have guidance for deploying these services using AWS ECS. You can find this guidance and
example task/service definitions in the `aws_deployment_guidance` folder.
