# Deploying Compliance Hub in AWS

This guidance provides an example how Compliance Hub services can be deployed with AWS. This
example uses AWS ECS and AWS Fargate to run the container images and a AWS ALB (Application
Load Balancer) which directs traffic to the appropriate container.

![Compliance Hub AWS Architecture Schematic](/assets/awsDeploymentSchematic.png)

In this deployment there are **3 ECS tasks** (in grey in diagram above) which define the
containers (containers are run using AWS Fargate):

- **client-ecs-task:** This AWS ECS task stores information on running of the
  compliance-hub-client container with AWS Fargate. An example task definition can be found in
  the file `client-ecs-task-def-example.json` in this folder.

- **gateway-ecs-task:** This AWS ECS task stores information on running compliance-hub-gateway and
  compliance-hub-clamav containers which serve the Compliance Hub APIs. An example task definition
  can be found in the file `gateway-ecs-task-def-example.json` in this folder. This task definition
  also includes the environment variables required by the compliance-hub-gateway container. Some of
  these environment variables are secret values (e.g. the password for the CS2 Oracle DB) and so
  these are stored using AWS secrets manager. ECS retrieves these secrets when the container is
  launched.

- **controller-ecs-task:** This AWS ECS task stores information on running of the
  compliance-hub-controller container. This task/container is executed every 5 minutes by an AWS
  EventBridge schedule. An example task definition can be found in the file
  `controller-ecs-task-def-example.json` in this folder. This task definition
  also includes the environment variables required by the compliance-hub-controller container.

This deployment has **2 ECS services** (in black in the diagram above) which associate ECS tasks
with the AWS ALB (Application Load Balancer):

- **client-ecs-service:** This ECS (AWS Elastic Container Service) service links the ALB to the
  `client-ecs-task` ECS task. An example service definition can be found in the file
  `client-ecs-service-def-example.json` in this folder.

- **gateway-ecs-service:** This ECS service links the ALB to the `gateway-ecs-task` ECS task. An
  example service definition can be found in the file `gateway-ecs-service-def-example.json` in
  this folder.

This deployment has an **AWS Application Load Balancer (ALB)** which is linked to the ECS services
as detailed above. This ALB has routing rules which determine which target group/container user
traffic is routed to. Example ALB listener rules can be found in the file
`alb-example-listener-rules.json`. Example ALB target groups which define where traffic is routed
to can be found in the file `alb-example-target-groups.json`.

## Accessing Scitegrity container images in AWS

Our container images are located in a private repository and credentials are required to download
these images. Please see the [AWS Documentation](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/private-auth.html)
for guidance on accessing private container repositories in AWS ECS.
